# Security Products test projects

[[_TOC_]]

This repo contains documentation about Security Products test projects.
It also provides a template to easily bootstrap a new test project for Security Products
e.g. when adding support for new languages or frameworks.

## How to use a Test Project

Please keep in mind that test projects are used by automated scripts for testing and
[QA](https://gitlab.com/gitlab-org/security-products/release/blob/master/docs/qa_process.md).
If you have any doubt please ask a ~Secure member.

**Caution:** We should not blindly update QA projects when expectations change. Ideally,
these updates are due to new/better vulnerability information but they can also hide
regressions and behavioral changes, such as sorting order or removal of previously
seen vulnerabilities. These QA expectation update MRs should be carefully reviewed.

Branches with the `-FREEZE` suffix are reserved for QA testing. When raising a merge 
request against a `-FREEZE` branch, the source branch should not include the `-FREEZE` 
suffix.

You can create new Branches and Merge Requests on this project for your own testing purpose
but you should not update nor delete `master` or any branch with the `-FREEZE` suffix.

Maintaining the `-FREEZE` branches across all the test projects is expensive,
so you are encouraged to **reuse existing branches** instead of creating new ones.
In particular, there is no need to create a new branch that configures the scanner in a particular way, using CI variables.
Instead, CI variables should be set when triggering the pipeline,
either from the pipeline of the analyzer project or from the Pipeline Schedule.

**Caution:** do not forget to update `-FREEZE` branches with latest changes to `master` when `master` is updated.

Please think about cleaning up your test branches and Merge Requests when you're done.

**Tip:** If you want to test a specific Merge Request behavior that depends on the state of the
target branch then you'd better create a specific target branch instead of using `master`.

### Known testing branches

* `master` tests multiple GitLab security scanners, like DAST, Dependency Scanning, and SAST. The CI configuration enables both the scanners and the corresponding QA jobs.
* `offline-FREEZE` ensures that scanners can execute in an offline environment. The CI configuration performs special setup before running the scanners: it prepares the codebase, installs iptables, and drops all traffic.
* `auto-devops-FREEZE` ensures that scanners are compatible with [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/). There's no CI configuration, and the pipeline has no QA jobs.

## How to create a new Test Project

### Project visibility

Please ensure that the project visibility level is [public](https://docs.gitlab.com/ee/user/public_access.html#public-projects-and-groups), so "The project can be accessed without any authentication".

### Project naming convention

Please name your project after: `language`-`package manager`-`framework` (remove any non relevant element).

### Project content

Copy the [template](template) content to the new repository and find/replace the `REPLACE_ME` placeholders.

- setup the `master` branch:
  - create a basic app for given language, package manager and framework with some relevant vulnerabilities
  - configure the compatible Security Products features in the `.gitlab-ci.yml` (comment out unsupported ones)
  - update the expected reports in `qa/expect/`
- create the `auto-devops-FREEZE` branch from `master` and removes the `.gitlab-ci.yml` file.
- Set the `*-FREEZE` branches as `Protected` under the project's "Repository Settings"

### Project configuration

### Topic

To easily find relevant test projects compatible with a given feature, we're using project's topics.

You can configure them in the main settings of the project:

![](docs/img/project_topics.png)

Once configured with topics, the corresponding project can be retrieved with these links:

- SAST: https://gitlab.com/explore/projects?tag=GL-Secure-QA,SAST
- DAST: https://gitlab.com/explore/projects?tag=GL-Secure-QA,DAST
- Dependency Scanning: https://gitlab.com/explore/projects?tag=GL-Secure-QA,Dependency+Scanning
- License Management: https://gitlab.com/explore/projects?tag=GL-Secure-QA,License+Management
- Container Scanning: https://gitlab.com/explore/projects?tag=GL-Secure-QA,Container+Scanning

NB: all projects should have `GL-Secure-QA` topic since there is currently no way to filter projects by group there.

## QA

### Secure Test Project Orchestrator

The [Secure Test Project Orchestrator](https://gitlab.com/gitlab-org/quality/ci/secure-test-project-orchestrator) is responsible for triggering the pipelines of the test projects on a regular basis, and notifying maintainers of the Secure analyzers when these pipelines fail.
It creates pipelines for all test projects, for `master` and all branches with the `-FREEZE` suffix.

### Secure analyzers

Each change to an analyzer project in Secure is tested against a test project matching the language or framework supported by this analyzer. For example, the `gemnasium-maven` analyzer for `java` will test the new changes against several different dependency frameworks and flavors of java: [gemnasium-maven downstream tests](https://gitlab.com/gitlab-org/security-products/analyzers/gemnasium-maven/-/blob/v2.18.4/.gitlab-ci.yml#L36-178).

The job triggering a downstream project verifies whether the output of the analyzer has changed between commits. If a change is unexpected, the pipeline will fail. Roughly, this process looks like:

1. code change is committed
1. a `tmp` image is built for this commit
1. downstream (`qa` stage) tests are triggered
1. the `tmp` image is used to analyze the test repo
1. the output of the analyzer's report is compared with the expected report, if they do not match the pipeline fails

The expectations for the `Dependency Scanning`, `SAST`, and `Secrets` analyzers are stored in `qa/expect` with a sub-directory matching the downstream project's framework/language type (eg. [qa/expect/java-maven](https://gitlab.com/gitlab-org/security-products/analyzers/gemnasium-maven/-/tree/master/qa/expect/java-maven)). If a change in the analyzer modifies its report, then the expectation must change as well.

When adding a new language or framework functionality, a new test project [needs to be created](https://gitlab.com/gitlab-org/security-products/tests/common/-/blob/master/README.md#how-to-create-a-new-test-project).

To add a scan duration performance check for an analyzer build, add the `MAX_SCAN_DURATION_SECONDS` environment variable to the downstream configuraton in the analyzer's `.gitlab-ci.yml` (e.g. https://gitlab.com/gitlab-org/security-products/analyzers/gemnasium/-/blob/master/.gitlab-ci.yml#L44 and https://gitlab.com/gitlab-org/security-products/ci-templates/-/blob/master/includes-dev/qa-dependency_scanning.yml#L122).

##### Configuring vulnerability database

Gemnasium analyzer's vulnerability data source can be configured using [environment variables](https://docs.gitlab.com/ee/user/application_security/dependency_scanning/#available-variables). This allows to pin the database to a specific version and make our tests more stable by preventing frequently changing expectations.
Currently we just update `GEMNASIUM_DB_REF_NAME` variable every month. `GEMNASIUM_DB_REF_NAME` points to a tag in [gemnasium-db](https://gitlab.com/gitlab-org/security-products/gemnasium-db/). Format of the `GEMNASIUM_DB_REF_NAME` is `vX.Y.Z`.

A similar approach is used for `bundler-audit` analyzer using the `BUNDLER_AUDIT_ADVISORY_DB_REF_NAME` environment variable. This points to a git ref in the analyzer's ruby advisory db.

## FIPS verification

Some Secure analyzers ship with FIPS-compliant variants that are compiled and
executed in a [RHEL UBI](https://developers.redhat.com/products/rhel/ubi)
based environment (images suffixed with `-fips`). These analyzers run against
approved system libraries and cryptographic algorithms, which are configured
upwards from the host kernel. You can read more on [how Red Hat Enterprise Linux
8 is designed for FIPS 140-2
requirements](https://www.redhat.com/en/blog/how-rhel-8-designed-fips-140-2-requirements).

### Checking FIPS mode

You can demonstrate that the FIPS mode is enabled by [executing the
following
command](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/security_hardening/assembly_installing-a-rhel-8-system-with-fips-mode-enabled_security-hardening)
(see the `Verification` section in the document):

``` shell
$ fips-mode-setup --check | grep "is enabled"
FIPS mode is enabled.
```

This command runs the `fips-mode-setup --check` utility (which executes `cat
/proc/sys/crypto/fips_enabled` and prints a friendly message) and will exit with
a non-zero code if `FIPS mode is enabled` is not present in the output. It
should be run when testing FIPS-compliant images to prove that FIPS mode is
enabled.

The following CI configuration ensures that the FIPS variants of the `semgrep`,
`secrets`, and `kics` analyzer images successfully runs in FIPS mode on a
FIPS-enabled host runner:

``` yaml
verify-fips-enabled:
  tags:
    - fips
    - aws
  rules:
    - when: always
  stage: test
  variables:
    ANALYZER_IMAGE: "registry.gitlab.com/security-products/$IMAGE_NAME"
  script: |
    fips-mode-setup --check | grep "is enabled"
  image:
    name: "$ANALYZER_IMAGE"
  parallel:
    matrix:
      - IMAGE_NAME: ["semgrep:3-fips", "secrets:4-fips", "kics:3-fips"]
```

### Running analyzers in a FIPS environment

The test projects can be tested with runners where FIPS is enabled and enforced.
To do so, set the job tags to `fips` and `aws`. For instance, the following CI
configuration runs the Dependency Scanning jobs using FIPS-enabled runners:

``` yaml
include:
  - template: Dependency-Scanning.gitlab-ci.yml

dependency_scanning:
  variables:
    CI_GITLAB_FIPS_MODE: "true"
  tags:
    - fips
    - aws
```

`CI_GITLAB_FIPS_MODE` is set to emulate the FIPS mode, so that Dependency
Scanning jobs use Docker images based on RHEL UBI and compliant with FIPS.

Today the FIPS-enabled runners are configured for the `tests` project group.
However, eventually `tests` should use FIPS-enabled shared runners registered
for `gitlab-org`, as part of
[GitLab#356232](https://gitlab.com/gitlab-org/gitlab/-/issues/356232); the
`tags` required to select these shared runners might be different.

## Contributing

If you want to help and contribute, read the [contribution guidelines](CONTRIBUTING.md).
