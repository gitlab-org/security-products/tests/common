# REPLACE_ME

Test project with:

* **Language:** REPLACE_ME
* **Package Manager:** REPLACE_ME
* **Framework:** REPLACE_ME

## How to use

Please see the [usage documentation](https://gitlab.com/gitlab-org/security-products/tests/common#how-to-use-a-test-project) for Security Products test projects.

## Supported Security Products Features

| Feature             | Supported                 |
|---------------------|---------------------------|
| SAST                | :white_check_mark: OR :x: |
| Dependency Scanning | :white_check_mark: OR :x: |
| Container Scanning  | :white_check_mark: OR :x: |
| DAST                | :white_check_mark: OR :x: |
| License Management  | :white_check_mark: OR :x: |
